#include <TM1637Display.h>

#include "mega2560_pinout.h"

TM1637Display disp(SEGMENT_DISPLAY_CLK_PIN, SEGMENT_DISPLAY_DIO_PIN);

int timer = 0;

void setup()
{
 /* pinMode(BUZZER_PIN, OUTPUT);
  for (int buzztimer = 0; buzztimer < 5; buzztimer++)
  {
    digitalWrite(BUZZER_PIN, HIGH);
    delay(500);
    digitalWrite(BUZZER_PIN, LOW);
    delay(500);
   
  }
  */
  disp.setBrightness(7);
  disp.clear();
  Serial.begin (9600);
}


void loop()
{
  delay(1000);
  timer ++;
  if (timer < 61);
  Serial.println(timer);
  disp.showNumberDec(timer,true);
}
