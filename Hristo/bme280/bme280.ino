#include "mega2560_pinout.h"

#include <Wire.h>

#include <Adafruit_BME280.h>

#define TCA_addr 0x70
#define BME_addr 0x76
#define BME_ch 0x00
Adafruit_BME280 bme;

void Chanel_select(uint8_t ch) {
  if(ch>7) return;
  Wire.beginTransmission(TCA_addr); 
  Wire.write(1 << ch);
  Wire.endTransmission();
  }

void setup() {
  Wire.begin();
  Serial.begin(9600);
  Chanel_select(BME_ch);
}

void loop() {
  Serial.print("temp");
  Serial.print(bme.readTemperature());
  Serial.println("C");
  delay(500);
}
