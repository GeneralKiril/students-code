#include "mega2560_pinout.h"


void setup() {
  pinMode(DIP1_PIN, INPUT);
  pinMode(DIP2_PIN, INPUT);
  pinMode(DIP3_PIN, INPUT);
  pinMode(DIP4_PIN, INPUT);
  Serial.begin(9600);
  pinMode(TMP36_T_SENSOR_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);
}
int old = 0;
int current = 0;
int start = 0;
void loop() {
current = millis();
int interval;
interval = current - old;
  bool dip_1 = digitalRead(DIP1_PIN);
  bool dip_2 = digitalRead(DIP2_PIN);
  bool dip_3 = digitalRead(DIP3_PIN);
  bool dip_4 = digitalRead(DIP4_PIN);

  if (dip_1 == 1)

  {

    int t = analogRead(TMP36_T_SENSOR_PIN);
    float mv = ( t / 1024.0) * 4900;
    float cel = mv / 10;
    Serial.print("val=");
    Serial.print(t);
    Serial.print(" T=");
    Serial.println(cel);
    delay(1000);
  }
  else if (dip_2 == 1)

  {

    digitalWrite(BUZZER_PIN, HIGH);
    delay(1000);
    digitalWrite(BUZZER_PIN, LOW);
    delay(1000);
    Serial.println(dip_2);

  }
    else if (dip_3 == 1)
  {

   if (interval > 1000)

   {
    start++;
    Serial.print("Timer:");
    Serial.println(start);
    old = current;
    
   }
  }
  
}
