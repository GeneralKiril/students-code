#define Motor_A1 11
#define Motor_A2 5
#define Motor_B3 9
#define Motor_B4 10
#define trigPin 7
#define echoPin 8
#define BT_RX 3
#define BT_TX 2
#define low_speed 50

#define RGB_Matrix A0

#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>
#include "VirtuinoBluetooth.h"

SoftwareSerial bluetoothSerial(BT_RX, BT_TX);
VirtuinoBluetooth virtuino(bluetoothSerial);
Adafruit_NeoPixel matrix(64, RGB_Matrix, NEO_GRB + NEO_KHZ800);

int pixelNum = 0;

void setup() {
  Serial.begin(9600);

  bluetoothSerial.begin(9600);
  pinMode(Motor_A1, OUTPUT);
  pinMode(Motor_A2, OUTPUT);
  pinMode(Motor_B3, OUTPUT);
  pinMode(Motor_B4, OUTPUT);
  pinMode(RGB_Matrix, OUTPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  matrix.begin();
  matrix.setBrightness(60);

}

int duration = 0, distance = 0;


void loop() {
  virtuino.run();

digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration * 0.034 / 2;

  int v5 = virtuino.vMemoryRead(5);
  /*
    Serial.print("v5=");
    Serial.println(virtuino.vMemoryRead(5));
    delay(100);
  */
  if (v5 == 1)
  {
    pixelNum = 25;
    matrix.setPixelColor(pixelNum, 255, 255, 255);
    pixelNum = 26;
    matrix.setPixelColor(pixelNum, 255, 255, 255);
    pixelNum = 29;
    matrix.setPixelColor(pixelNum, 255, 255, 255);
    pixelNum = 30;
    matrix.setPixelColor(pixelNum, 255, 255, 255);
    pixelNum = 33;
    matrix.setPixelColor(pixelNum, 255, 255, 255);
    pixelNum = 34;
    matrix.setPixelColor(pixelNum, 255, 255, 255);
    pixelNum = 37;
    matrix.setPixelColor(pixelNum, 255, 255, 255);
    pixelNum = 38;
    matrix.setPixelColor(pixelNum, 255, 255, 255);

  }
  else {
    pixelNum = 0;
    matrix.clear();
  }

  //25,26,29,30,33,34,37,38,

  matrix.show();
  delay(100);

  if (virtuino.vMemoryRead(1) == 1 && distance > 20)
  {
    motors_on();

  }

  else if (virtuino.vMemoryRead(2) == 1)
  {
    motors_left();
  }

  else if (virtuino.vMemoryRead(3) == 1)
  {
    motors_right();
  }

  else if (virtuino.vMemoryRead(4) == 1)
  {
    motors_back();
  }

  else
  {
    motors_stop();
  }



}

void motors_on()
{
  digitalWrite(Motor_A1, HIGH);
  digitalWrite(Motor_A2, LOW);
  digitalWrite(Motor_B3, HIGH);
  digitalWrite(Motor_B4, LOW);
}

void motors_back()
{
  digitalWrite(Motor_A1, LOW);
  digitalWrite(Motor_A2, HIGH);
  digitalWrite(Motor_B3, LOW);
  digitalWrite(Motor_B4, HIGH);
}

void motors_stop()
{
  digitalWrite(Motor_A1, LOW);
  digitalWrite(Motor_A2, LOW);
  digitalWrite(Motor_B3, LOW);
  digitalWrite(Motor_B4, LOW);
}

void motors_left()
{
  analogWrite(Motor_A1, LOW);
  analogWrite(Motor_A2, low_speed);
  analogWrite(Motor_B3, low_speed);
  analogWrite(Motor_B4, LOW);
}

void motors_right()
{
  analogWrite(Motor_A1, low_speed);
  analogWrite(Motor_A2, LOW);
  analogWrite(Motor_B3, LOW);
  analogWrite(Motor_B4, low_speed);
}
