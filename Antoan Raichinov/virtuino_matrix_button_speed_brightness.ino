#include <Adafruit_NeoPixel.h>

#define RGB_Matrix A0
#define BT_RX 3
#define BT_TX 2

#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>
#include "VirtuinoBluetooth.h"

Adafruit_NeoPixel matrix(64, RGB_Matrix, NEO_GRB + NEO_KHZ800);
SoftwareSerial bluetooth (3, 2);
VirtuinoBluetooth virtuino(bluetooth);

void setup() {
  pinMode(RGB_Matrix, OUTPUT);
  matrix.setBrightness(60);
  matrix.begin();
  bluetooth.begin(9600);
}
int pixelNum = 0;
int travel_speed = 50;
int Brightness = 60;

void loop() {
  virtuino.run();

  Brightness = virtuino.vMemoryRead(3);
  matrix.setBrightness(Brightness);
  
  travel_speed = virtuino.vMemoryRead(2);
  if (pixelNum > 63) {
    matrix.clear();
    pixelNum = 0;
  }
  if (virtuino.vMemoryRead(1) == 1) {
    matrix.setPixelColor(pixelNum, 23, 34, 45);
    matrix.show();
    pixelNum++;
    delay(travel_speed);

  }
}
