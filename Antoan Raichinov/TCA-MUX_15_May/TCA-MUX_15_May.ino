#include <Adafruit_BME280.h>
#include <Wire.h>
#include "mega2560_pinout.h"

#define TSA_ADDR 0x70
#define BME280_CH 0x00
#define BME280_ADDR 0x76

Adafruit_BME280 bme;

void MUX_Select(uint8_t ch) {
  if (ch > 7) {
    Serial.println("Error");
    return;
  }
  Wire.beginTransmission(TSA_ADDR);
  Wire.write(1 << ch);
  Wire.endTransmission();
}

void setup() {
  Wire.begin();
  Serial.begin(9600);
  MUX_Select(BME280_CH);

  bme.begin(BME280_ADDR);
}

void loop() {

  Serial.print("Temp ");
  Serial.print(bme.readTemperature());
  Serial.println(" C");

}
