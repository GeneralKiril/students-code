#include <Adafruit_NeoPixel.h>


#include "VirtuinoBluetooth.h"
#include "mega2560_pinout.h"

VirtuinoBluetooth virtuino(Serial1);
#include <DHT.h>

#include "mega2560_pinout.h"
//#define WP22 (22)
//#define DP22 WP22
//#define DHT11_PIN WP22

#define DHTTYPE DHT11   // DHT 11

DHT dht(DHT11_PIN, DHTTYPE);

// Initialize all needed pins and libraries
// Initialize all needed pins and libraries
void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);

  Serial.println("Start");
  // put your setup code here, to run once:
  Serial.begin(9600);

  dht.begin();
}

int mem1 = 0;

// Write the code executed in a loop
void loop() {
  virtuino.run();
  delay(2000);
  int h = dht.readHumidity();        // read humidity
  int t = dht.readTemperature();     // read temperature
  virtuino.vMemoryWrite(7, h);
  virtuino.vMemoryWrite(8, t);
}
