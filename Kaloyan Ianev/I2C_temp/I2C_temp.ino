#include <Wire.h>
#include <Temperature_LM75_Derived.h>
#include "mega2560_pinout.h"
#define TCA_ADDR 0x70
#define LM75A_TEMP_SENSOR_ADDR 0x48
#define tempch 2
Generic_LM75 temp(LM75A_TEMP_SENSOR_ADDR);

void I2C_hannel(uint8_t ch) {
  if (ch > 7) return;

  Wire.beginTransmission(TCA_ADDR);
  Wire.write(ch << 1);
  Wire.endTransmission();
}

void setup() {
  Serial.begin(9600);
  Wire.begin();
  I2C_hannel(tempch);
}
void loop() {
  Serial.print("the temperature is:");
  Serial.println(temp.readTemperatureC());
  delay(1000);


}
