#define motor_a_in1 11
#define motor_a_in2 5
#define motor_b_in1 9
#define motor_b_in2 10
#define BT_RX 3
#define BT_TX 2

#include <SoftwareSerial.h>
SoftwareSerial bluetooth(BT_RX, BT_TX);
#include"VirtuinoBluetooth.h"
VirtuinoBluetooth virtuino(bluetooth);

void setup() {
  bluetooth.begin(9600);
  pinMode(motor_a_in1, OUTPUT);
  pinMode(motor_a_in2, OUTPUT);
  pinMode(motor_b_in1, OUTPUT);
  pinMode(motor_b_in2, OUTPUT);
}

void loop() {
  virtuino.run();
  if (virtuino.vMemoryRead(1) == 1)
  {
    forward();
  }
  else if (virtuino.vMemoryRead(2) == 1)
  {
    backward();
  }
  else if (virtuino.vMemoryRead(3) == 1)
  {
    left();
  }
  
  else if (virtuino.vMemoryRead(4) == 1)
  {
    right();
  }
  else
  {
    mstop();
  }

}
void forward() {
  digitalWrite (motor_a_in1, HIGH);
  digitalWrite (motor_a_in2, LOW);
  digitalWrite (motor_b_in1, HIGH);
  digitalWrite (motor_b_in2, LOW);
}
void backward() {
  digitalWrite (motor_a_in1, LOW);
  digitalWrite (motor_a_in2, HIGH);
  digitalWrite (motor_b_in1, LOW);
  digitalWrite (motor_b_in2, HIGH);
}
void right() {
  digitalWrite (motor_a_in1, HIGH);
  digitalWrite (motor_a_in2, LOW);
  digitalWrite (motor_b_in1, LOW);
  digitalWrite (motor_b_in2, HIGH);
}
void left() {
  digitalWrite (motor_a_in1, LOW);
  digitalWrite (motor_a_in2, HIGH);
  digitalWrite (motor_b_in1, HIGH);
  digitalWrite (motor_b_in2, LOW);
}
void mstop() {
  digitalWrite (motor_a_in1, LOW);
  digitalWrite (motor_a_in2, LOW);
  digitalWrite (motor_b_in1, LOW);
  digitalWrite (motor_b_in2, LOW);
}
