#define TX 2
#define RX 3
#define LDR A6

#include <SoftwareSerial.h>
SoftwareSerial bluetoothSerial(RX, TX);

void setup() {

  Serial.begin(9600);
  bluetoothSerial.begin(9600);
  pinMode(LDR, OUTPUT);

}

void loop() {


  if( bluetoothSerial.available() ) {
    Serial.write(bluetoothSerial.read());    
  }

    if( Serial.available() ) {
    bluetoothSerial.write(Serial.read());    
  }

}
