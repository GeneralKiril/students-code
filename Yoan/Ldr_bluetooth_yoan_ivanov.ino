#define TX 2
#define RX 3
#define LDR A6

#include <SoftwareSerial.h>
SoftwareSerial bluetoothSerial(RX, TX);

void setup() {

  Serial.begin(9600);
  bluetoothSerial.begin(9600);
  pinMode(LDR, OUTPUT);

}

void loop() {
  int number = 0;
  int data[10];
  for (int w = 0; w < 10; w ++) {
    data [w] = analogRead (LDR);
    number+= data[w];
  }

  number = number/10;

  bluetoothSerial.println(number);
  delay(50);


}
