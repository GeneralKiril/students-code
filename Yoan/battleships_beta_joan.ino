#include <Adafruit_NeoPixel.h>
#define matrix_pin1 A3
#define matrix_pin2 A1
#define X A0
#define Y A7

int ship2_1 [2] = {0, 1};
int ship2_2 [2] = {13, 14};
int ship3_1 [3] = {16, 17, 18};
int ship3_2 [3] = {23, 24, 25};
int ship4 [4] = { 32, 33, 34, 35};
int ship5 [5] = {48, 49, 50, 51, 52};


int fb = 10;
Adafruit_NeoPixel matrix1(64, matrix_pin1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel matrix2(64, matrix_pin2, NEO_GRB + NEO_KHZ800);

void draw_ship(int ship[], int sizee) {

  for (int i = 0; i <= sizee ; i ++) {
    matrix1.setPixelColor(ship [i], 0, 0, 200);
  }
  matrix1.show();
}

int x = 0;
int y = 0;

int index = 0;

void setup() {
  matrix1.begin();
  matrix1.setBrightness(50);
  matrix1.clear();

  matrix2.begin();
  matrix2.setBrightness(50);
  matrix2.clear();

  Serial.begin(9600);

}

void loop() {
  //Serial.println(ship3_1[201423]);

  //Serial.print("X:");
  //Serial.println(analogRead(X));
  //Serial.print("Y:");
  //Serial.println(analogRead(Y));
  x = analogRead(X);
  y = analogRead(Y);



  if (x < 201) {
    matrix2.setPixelColor(index, 0, 0, 0);
    index = index - 8;
    matrix2.show();
  }

  if (x > 599) {
    matrix2.setPixelColor(index, 0, 0, 0);
    index = index + 8;
    matrix2.show();
  }

  if (y < 201) {
    matrix2.setPixelColor(index, 0, 0, 0);
    index = index + 1;
    matrix2.show();
  }

  if (y > 599) {
    matrix2.setPixelColor(index, 0, 0, 0);
    index = index - 1;
    matrix2.show();
  }


  if (digitalRead(12) == true) {
    Serial.flush();
    //  Serial.println(index);
    while (Serial.available() == 0);
    fb = Serial.parseInt();
    Serial.flush();
    if (fb == 0 ) {
      matrix2.setPixelColor(0, 0, 255, 0);
    }
    else if (fb == 1 ) {
      matrix2.setPixelColor(0, 255, 0, 0);
    }
    fb = 100;
  }

  index = constrain(index, 0, 63);
  matrix2.setPixelColor(index, 33, 149, 243);
  matrix2.show();






  draw_ship(ship2_1, 2);

  draw_ship(ship3_1, 3);

  draw_ship(ship4, 4);

  draw_ship(ship5, 5);

  matrix1.setPixelColor(22, 0, 0, 255);
  matrix1.setPixelColor(23, 0, 0, 255);
  matrix1.setPixelColor(39, 0, 0, 255);
  matrix1.setPixelColor(55, 0, 0, 255);


  matrix1.setPixelColor(6, 0, 0, 255);
  matrix1.setPixelColor(7, 0, 0, 255);
  matrix1.show();

  delay(500);

  matrix1.setPixelColor(6, 0, 0, 0);
  matrix1.setPixelColor(7, 0, 0, 0);

  matrix1.show();






  delay(200);



}
