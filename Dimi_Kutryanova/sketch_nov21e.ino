#define TX 2
#define RX 3

#include<SoftwareSerial.h>
SoftwareSerial bluetooth(RX, TX);

void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);
}

void loop() {
  if(bluetooth.available()){
  Serial.write(bluetooth.read());
  }
}
