#define TX 2
#define RX 3
#define buzz 6
#define LED 13

#include<SoftwareSerial.h>
SoftwareSerial bluetooth(RX, TX);

void dash() {
  digitalWrite(buzz, HIGH);
  digitalWrite(LED, HIGH);
  delay(300);
  digitalWrite(buzz, LOW);
  digitalWrite(LED, LOW);
  bluetooth.print('-');
}

void dot() {
  digitalWrite(buzz, HIGH);
  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(buzz, LOW);
  digitalWrite(LED, LOW);
  bluetooth.print('.');
}

void s() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void o() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void y() {
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void a() {
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void n() {
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void e() {
  dot();
  bluetooth.print("  ");
  delay(300);
}

void g() {
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void h() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void m() {
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void t() {
  dash();
  bluetooth.print("  ");
  delay(300);
}

void u() {
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void v() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void w() {
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void b() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void c() {
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void d() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void f() {
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void i() {
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void j() {
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void k() {
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void l() {
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void p() {
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void q() {
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void r() {
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void x() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void z() {
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}







/*void bg_ch() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(300);
}

void bg_f() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(300);
}

void bg_y() {
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(300);
}

void bg_q() {
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(300);
}*/





void space() {
  delay(400);
  bluetooth.print("     ");
}





void chis_1() {
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void chis_2() {
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void chis_3() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void chis_4() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void chis_5() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void chis_6() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void chis_7() {
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void chis_8() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void chis_9() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void chis_0() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}







void tochka() {
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void zapetaya() {
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void vapros() {
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void apostrof() {
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void udivitelen() {
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void naklon() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void skoba_lqva() {
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void skoba_dqsna() {
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void zavartyano_i() {
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void dvoetochie() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void diez() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void tochka_zapetaya() {
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void ravno() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void tire() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void dolna() {
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void kavichki() {
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void dolari() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dash();
  bluetooth.print("  ");
  delay(300);
}

void maimuna() {
  dot();
  delay(100);
  dash();
  delay(100);
  dash();
  delay(100);
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
  bluetooth.print("  ");
  delay(300);
}

void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);

  pinMode(buzz, OUTPUT);
  pinMode(LED, OUTPUT);
}

char var;

void loop() {
  
  if (bluetooth.available()) {
    var = bluetooth.read();
    if (var == 'a' || var == 'A') {
      a();
    }
    else if (var == 'b' || var == 'B') {
      b();
    }
    else if (var == 'c' || var == 'C') {
      c();
    }
    else if (var == 'd' || var == 'D') {
      d();
    }
    else if (var == 'e' || var == 'E') {
      e();
    }
    else if (var == 'f' || var == 'F') {
      f();
    }
    else if (var == 'g' || var == 'G') {
      g();
    }
    else if (var == 'h' || var == 'H') {
      h();
    }
    else if (var == 'i' || var == 'I') {
      i();
    }
    else if (var == 'j'|| var == 'J') {
      j();
    }
    else if (var == 'k' || var == 'K') {
      k();
    }
    else if (var == 'l' || var == 'L') {
      l();
    }
    else if (var == 'm' || var == 'M') {
      m();
    }
    else if (var == 'n' || var == 'N') {
      n();
    }
    else if (var == 'o' || var == 'O') {
      o();
    }
    else if (var == 'p' || var == 'P') {
      p();
    }
    else if (var == 'q' || var == 'Q') {
      q();
    }
    else if (var == 'r' || var == 'R') {
      r();
    }
    else if (var == 's' || var == 'S') {
      s();
    }
    else if (var == 't' || var == 'T') {
      t();
    }
    else if (var == 'u' || var == 'U') {
      u();
    }
    else if (var == 'v' || var == 'V') {
      v();
    }
    else if (var == 'w' || var == 'W') {
      w();
    }
    else if (var == 'x' || var == 'X') {
      x();
    }
    else if (var == 'y' || var == 'Y') {
      y();
    }
    else if (var == 'z' || var == 'Z') {
      z();
    }
    /*else if (var == 'ч') {
      bg_ch();
    }
    else if (var == 'ш') {
      bg_f();
    }
    else if (var == 'ю') {
      bg_y();
    }
    else if (var == 'я') {
      bg_q();
    }*/
    else if (var == '1') {
      chis_1();
    }
    else if (var == '2') {
      chis_2();
    } else if (var == '3') {
      chis_3();
    }
    else if (var == '4') {
      chis_4();
    }
    else if (var == '5') {
      chis_5();
    }
    else if (var == '6') {
      chis_6();
    }
    else if (var == '7') {
      chis_7();
    }
    else if (var == '8') {
      chis_8();
    }
    else if (var == '9') {
      chis_9();
    }
    else if (var == '0') {
      chis_0();
    }
    else if (var == '.') {
      tochka();
    }
    else if (var == ',') {
      zapetaya();
    }
    else if (var == '?') {
      vapros();
    }
    else if (var == '\'' ) {
      apostrof();
    }
    else if (var == '!') {
      udivitelen();
    }
    else if (var == '/') {
      naklon();
    }
    else if (var == '(') {
      skoba_lqva();
    }
    else if (var == ')') {
      skoba_dqsna();
    }
    else if (var == '&') {
      zavartyano_i();
    }
    else if (var == ':') {
      dvoetochie();
    }
    else if (var == ';') {
      tochka_zapetaya();
    }
    else if (var == '#') {
      diez();
    }
    else if (var == '=') {
      ravno();
    }
    else if (var == '-') {
      tire();
    }
    else if (var == '_') {
      dolna();
    }
    else if (var == '"') {
      kavichki();
    }
    else if (var == '$') {
      dolari();
    }
    else if (var == '@') {
      maimuna();
    }
    else if (var = ' ') {
      space();
    }
  }
  Serial.print(var);
}
