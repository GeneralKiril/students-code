#define TX 2
#define RX 3
#define LDR A6

#include <SoftwareSerial.h>
SoftwareSerial bluetooth(RX, TX);

void setup() {
  
   Serial.begin(9600);
  bluetooth.begin(9600);
  pinMode(A6,OUTPUT);
}

void loop() {
int sum = 0;

for (int index = 0;index <10;index ++
sum += analogRead(A6);
  
  bluetooth.println(sum/10 );
  delay(100);

}
