#include <Adafruit_NeoPixel.h>

#define matrix_pin0 A0
#define matrix_pin1 A1
#define jx A3
#define jy A1

Adafruit_NeoPixel myscreen(64, matrix_pin0, NEO_RGB + NEO_KHZ800);
Adafruit_NeoPixel shoot(64, matrix_pin1, NEO_RGB + NEO_KHZ800);

int dot = 0;
int dotpos = 10;
int ship2[2] = {0, 1};
int ship3[3] = {16, 17, 18};
int ship4[4] = {32, 33, 34, 35};
int ship5[5] = {48, 49, 50, 51, 52};

void drawship(int ship[], int golemina) {
  for (int i = 0; i <= golemina; i++) {
    myscreen.setPixelColor(ship[i], 200, 0, 0);
  }
  myscreen.show();
}

void setup() {
  Serial.begin(9600);
  myscreen.begin();
  myscreen.setBrightness(70);
  myscreen.clear();
  shoot.begin();
  shoot.setBrightness(70);
  shoot.clear();

}

void loop() {

  drawship(ship2, 2);
  drawship(ship3, 3);
  drawship(ship4, 4);
  drawship(ship5, 5);

  int x = analogRead(A3);
  int y = analogRead(A7);
  Serial.println(y);

  if (x < 300)
  {
    dot -= 8;
  }

  if (x > 529)
  {
    dot += 8;
  }

  if (y < 300)
  {
    dot -= 1;
  }

  if (y > 525)
  {
    dot += 1;
  }
  delay(20);



  shoot.setPixelColor(dot, 200, 200, 200);
  shoot.show();
  //Serial.println(dot);
}
