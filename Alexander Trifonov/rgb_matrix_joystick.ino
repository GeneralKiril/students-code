#include <Adafruit_NeoMatrix.h>
#include <SoftwareSerial.h>
#define rgbMatrix A0
#define VRx A3
#define VRy A7
#define SW A1
const int rgbMatrixPiexelCount = 64;
const int rgb_red = 8;

Adafruit_NeoPixel rgb_Matrix = Adafruit_NeoPixel(rgbMatrixPiexelCount, rgbMatrix, NEO_GRB + NEO_KHZ800);
void setup() {
  Serial.begin(9600);
  rgb_Matrix.begin();
  rgb_Matrix.setBrightness(50);
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(SW, INPUT_PULLUP);
}

void loop() {
  int x = analogRead(VRx);
  int y = analogRead(VRy);
  int btn = digitalRead(A7);
  int MatrixX = map(x, 0, 967, 0, 6);
  int MatrixY = map(y, 1023, 0, 0, 6);
  int red = random(0, 256);
  int green = random(0, 256);
  int blue = random(0, 256);
  
  int rgb = MatrixX + 8 * MatrixY;
  rgb_Matrix.setPixelColor(rgb, red, green, blue);
  rgb_Matrix.show();
  Serial.print(MatrixX);
  //Serial.print(x);
  Serial.print(", ");
  Serial.print(MatrixY);
  //Serial.print(y);
  Serial.print(", ");
  Serial.print(btn);
  Serial.println();
  delay(10);
}
