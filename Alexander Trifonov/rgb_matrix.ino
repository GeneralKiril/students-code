#include <Adafruit_NeoMatrix.h>
#define rgbMatrix A3
const int rgbMatrixPiexelCount = 64;

Adafruit_NeoPixel rgb_Matrix = Adafruit_NeoPixel(rgbMatrixPiexelCount, rgbMatrix, NEO_GRB + NEO_KHZ800);
void setup() {
  rgb_Matrix.begin();
  rgb_Matrix.setBrightness(20);

}

void loop() {
  int red = random(0, 256);
  int green = random(0, 256);
  int blue = random(0, 256);
  for (int i=0 ; i < 64; i++) {
    rgb_Matrix.setPixelColor(i, red, green, blue);
    rgb_Matrix.show();
    delay(12);
  }
}
