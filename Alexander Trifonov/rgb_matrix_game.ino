#include <Adafruit_NeoMatrix.h>
#include <SoftwareSerial.h>
#define rgbMatrix A0
#define VRx A1
#define VRy A7
#define SW A3
const int rgbMatrixPiexelCount = 64;
const int rgb_red = 8;
int rgb_x = 3;
int rgb_y = 3;
Adafruit_NeoPixel rgb_Matrix = Adafruit_NeoPixel(rgbMatrixPiexelCount, rgbMatrix, NEO_GRB + NEO_KHZ800);
void setup() {
  Serial.begin(9600);
  rgb_Matrix.begin();
  rgb_Matrix.setBrightness(50);
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(SW, INPUT_PULLUP);
}

void loop() {

  int x = analogRead(VRx);
  int y = analogRead(VRy);
  int btn = digitalRead(A7);
  int MatrixX = map(x, 0, 850, 0, 6);
  int MatrixY = map(y, 850, 0, 0, 6);
  int red = random(0, 256);
  int green = random(0, 256);
  int blue = random(0, 256);

  Serial.print(MatrixX);
  //Serial.print(x);
  Serial.print(", ");
  Serial.print(MatrixY);
  //Serial.print(y);
  Serial.print(", ");
  Serial.print(btn);
  Serial.println();
  delay(300);
  int rgb_index = rgb_x*8+rgb_y;
  if (x > 650) {
    rgb_Matrix.setPixelColor(rgb_index + 1, red, green, blue);
    rgb_Matrix.show();
  }
  if(x < 350){
    rgb_Matrix.setPixelColor(rgb_index - 1, red, green, blue);
    rgb_Matrix.show();
  }
  if(y > 650){
    rgb_Matrix.setPixelColor(rgb_index - 8, red, green, blue);
    rgb_Matrix.show();
  }
  if(y < 350){
    rgb_Matrix.setPixelColor(rgb_index + 8, red, green, blue);
    rgb_Matrix.show();
  }
  /*if(rgb_index > 6){
    rgb_Matrix.setPixelColor(rgb_x, red, green, blue);
    rgb_Matrix.setPixelColor(rgb_y, red, green, blue);
    rgb_Matrix.show();
  */
   
  rgb_Matrix.clear();
  }
