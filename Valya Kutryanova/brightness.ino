#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel pixel(1, 16, NEO_GBR + NEO_KHZ800);

void setup() {
  pinMode(12, INPUT);
  pixel.begin();
  pixel.setBrightness(0);
}

int pixel_brightness = 0;
int x = 0;

void loop() {
  pixel.setPixelColor(0, 150, 140, 30);
  pixel.show();
  

  if (digitalRead(12) == HIGH && x == 0) {
    x = 1;
    if (pixel_brightness >= 250) {
      pixel_brightness = 0;
    }
    else {
      pixel_brightness += 50;
    }
    pixel.setBrightness(pixel_brightness);
  }
  else if (digitalRead(12) == LOW) {
    x = 0;
  }

}
