#define TX 2
#define RX 3
#define LDR A6

#include<SoftwareSerial.h>
SoftwareSerial bluetooth(RX, TX);

void setup() {
  pinMode(A6, INPUT);
  Serial.begin(9600);
  bluetooth.begin(9600);

}

void loop() {
  int s = 0;
  for (int i = 0; i < 10; i++) {
    s += analogRead(A6);;
  }
  if (bluetooth.available()) {
    bluetooth.println(s/10);
  }
}
