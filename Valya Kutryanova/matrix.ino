#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel matrix(64, A0, NEO_GBR + NEO_KHZ800);

void setup() {
  pinMode(12, INPUT);
  pinMode(A0, OUTPUT);
  matrix.begin();
  matrix.setBrightness(70);

}

int matrix_brightness = 0;


void loop() {
  int x = 0;
  for (int i = 0; i <= 63; i++){
    matrix.setPixelColor(x, 150, 140, 30);
    matrix.show();
    delay(30);
    matrix.setPixelColor(x, 0, 0, 0);
    matrix.show();
    x++;
    }

}
