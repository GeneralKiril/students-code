#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel pixel(64, A0, NEO_GRB + NEO_KHZ800);
void setup() {
  pinMode(12, INPUT);
  pinMode(A0, OUTPUT);
  pixel.begin();
  pixel.setBrightness(15);
}
int pixelBrightness = 0;
void loop() {
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 255, 61, 61);    //100 , 150, 64
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 255, 135, 61);   //55 , 200, 88
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 255, 229, 61);
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 177, 255, 61);
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 61, 255, 100);
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 61, 249, 255);
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 61, 77, 255);
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 181, 61, 255);
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 255, 61, 252);
    pixel.show();
    //delay(100);
  }
  for (int i = 0; i < 64; i++) {
    pixel.setPixelColor(i, 255, 61, 158);
    pixel.show();
    //delay(100);
  }
}
