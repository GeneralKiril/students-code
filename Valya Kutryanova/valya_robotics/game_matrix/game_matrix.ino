#define l_d A1
#define g_d A7
#define button A3

#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel pixel(64, A0, NEO_GRB + NEO_KHZ800);
void setup() {
  pinMode(12, INPUT);
  pinMode(A0, OUTPUT);
  pixel.begin();
  pixel.setBrightness(15);

  pinMode(l_d, INPUT);
  pinMode(g_d, INPUT);
  pinMode(button, INPUT_PULLUP);

  Serial.begin(9600);
}
int pixelBrightness = 0;
void loop() {
  /*int tablica[8][8] ;
  int red = 4;
  int kolona = 3;
  pixel.setPixelColor(8 * red + kolona, 255, 255, 255);
  pixel.show();*/


  Serial.print("l_d - ");
  Serial.println(analogRead(l_d));
  Serial.print("g_d - ");
  Serial.println(analogRead(g_d));
  Serial.print("button - ");
  Serial.println(digitalRead(button));
  delay(1000);
}

void jump() {

}
