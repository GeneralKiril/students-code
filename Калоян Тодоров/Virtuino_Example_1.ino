#define Button 12
#define LED 13
#define LDR A6

#define BT_RX 3
#define BT_TX 2

#include <SoftwareSerial.h>
SoftwareSerial bluetoothSerial(BT_RX, BT_TX);

#include "VirtuinoBluetooth.h"
VirtuinoBluetooth virtuino(bluetoothSerial);

void setup() {
  bluetoothSerial.begin(9600);
  virtuino.vPinMode(LDR, INPUT);
  virtuino.vPinMode(Button, INPUT);
  virtuino.vPinMode(LED, OUTPUT);
}

void loop() {
  virtuino.run();

  int analogValue = analogRead(LDR);
  virtuino.vMemoryWrite(1, analogValue);

  int btn = digitalRead(Button);
  virtuino.vMemoryWrite(2, btn);

  int digitalMemoryValue = virtuino.vMemoryRead(3);
  digitalWrite(LED, digitalMemoryValue);
}
